﻿using CharactertoNumberConvert.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace CharactertoNumberConvert.Controllers
{
    public class CharactertoNumberController : ApiController
    {
        [HttpPost]
        public JsonResult<Output> CharactertoNumber(User user)
        {
            string harfler = user.UserText;

            string[] birler = { "sıfır", "bir", "iki", "üç", "dört", "beş", "altı", "yedi", "sekiz", "dokuz" };
            string[] onlar = { "sıfır", "on", "yirmi", "otuz", "kırk", "elli", "altmış", "yetmiş", "seksen", "doksan" };
            string[] karisik = { "yüz", "bin", "", "", "", "", "", "", "", "" };


            int binFlag = 0;
            string ybyKelime = "";
            int rakamMat = 0;
            int rakamMat2 = 0;
            string kelimeMat = " ";
            string tekHarf = "";
            string rakamdanOnce = "";
            string prev = "";
            int prevIndis = 0;
            int[] Indisler = new int[10];

            for (int i = 0; i < harfler.Length; i++)
            {
                ybyKelime += harfler[i];
                tekHarf = harfler[i].ToString();

                prev = harfler[i].ToString();
                prevIndis = i;

                if (tekHarf == " ")
                {
                    if (rakamMat < 1)
                    {
                        rakamdanOnce += ybyKelime;
                        ybyKelime = "";
                    }
                }

                if (ybyKelime == " ")
                {
                    ybyKelime = "";
                }


                if (ybyKelime.Contains(" "))
                {
                    kelimeMat = ybyKelime.ToString();
                }

                for (int j = 0; j < birler.Length; j++)
                {
                    if (birler[j] == ybyKelime.ToLower().ToString())
                    {
                        if (binFlag == 0)
                        {
                            rakamMat += j;
                        }
                        else if (binFlag == 1)
                        {
                            rakamMat2 += j;
                        }

                        ybyKelime = "";
                    }

                    if (onlar[j] == ybyKelime.ToLower().ToString())
                    {
                        if (binFlag == 0)
                        {
                            rakamMat += j * 10;
                        }
                        else if (binFlag == 1)
                        {
                            rakamMat2 += j * 10;
                        }

                        ybyKelime = "";

                    }

                    if (karisik[j] == ybyKelime.ToLower().ToString())
                    {

                        if (ybyKelime.ToLower().ToString() == "yüz")
                        {
                            if (binFlag == 0)
                            {
                                if (rakamMat == 0)
                                {
                                    rakamMat = 1;
                                }
                                rakamMat *= 100;
                            }
                            else if (binFlag == 1)
                            {
                                rakamMat2 *= 100;
                            }
                            ybyKelime = "";

                        }

                        if (ybyKelime.ToLower().ToString() == "bin")
                        {
                            if (rakamMat == 0)
                            {
                                rakamMat = 1;
                            }

                            rakamMat *= 1000;
                            ybyKelime = "";
                            binFlag = 1;
                        }

                    }
                }
            }

            Output op = new Output();

            if (rakamMat2 > 0)
            {
                op.OutPut = rakamdanOnce + (rakamMat + rakamMat2) + " " + kelimeMat.ToString();
            }
            else
            {
                op.OutPut = rakamdanOnce + rakamMat + " " + kelimeMat.ToString();
            }
         
            return Json(op);
        }
    }
}
